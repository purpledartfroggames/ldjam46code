﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    [SerializeField] float EnemySpeed;
    [SerializeField] EnumEnemyType EnemyType;

    LevelPathManager PathManager;
    Transform PathPoint;

    Vector2 pathway;

    int curIndex = 0;

    float SlowUntil = 0f;

    // Start is called before the first frame update
    void Start()
    {
        PathManager = GameObject.FindObjectOfType<LevelPathManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (PathManager == null)
            Destroy(this.gameObject);

        this.MoveOnPath();
    }

    void MoveOnPath()
    {
        if (PathPoint != null && Mathf.Abs(Vector2.Distance(this.transform.position, PathPoint.position)) < GameConfig.GC_Setup_PathDistanceTolerance)
        {
            this.transform.position = PathPoint.position;
            PathPoint = PathManager.GetPathPoint(curIndex);
            curIndex++;
        }

        if (PathPoint == null)
        {
            PathPoint = PathManager.GetPathPoint(curIndex);
            curIndex++;
        }

        if (PathPoint == null)
        {
            this.EndOfPath();
        }
        else
        {
            pathway = PathPoint.position - this.transform.position;
            pathway = pathway.normalized;

            if (SlowUntil > Time.time)
                pathway = pathway * GameConfig.GC_Ammo_SlowFactor;

            this.transform.Translate(pathway * EnemySpeed * Time.deltaTime);
        }
    }

    void EndOfPath()
    {
        ScoreManager.Instance.AdjustProjectFocus(-this.DamageFocusFromType(), false, true, this.transform.position.x, this.transform.position.y + 2f, this.transform.position.z);
        ScoreManager.Instance.AdjustProjectQuality(-this.DamageQualityFromType(), false, true, this.transform.position.x, this.transform.position.y + 1f, this.transform.position.z);

        AudioManager.Instance.PlaySoundInteraction("BrainDamage");

        Destroy(this.gameObject);
    }

    public Vector3 GetPathWay()
    {
        return pathway;
    }

    public EnumEnemyType GetEnemyType()
    {
        return EnemyType;
    }

    public int DamageFocusFromType()
    {
        int damage = 0;

        switch (EnemyType)
        {
            case EnumEnemyType.Bug: damage = GameConfig.GC_Enemy_Bug_Damage_Focus; break;
            case EnumEnemyType.Sun: damage = GameConfig.GC_Enemy_Sun_Damage_Focus; break;
            case EnumEnemyType.Sport: damage = GameConfig.GC_Enemy_Sport_Damage_Focus; break;
        }

        return damage;
    }

    public int DamageQualityFromType()
    {
        int damage = 0;

        switch (EnemyType)
        {
            case EnumEnemyType.Bug: damage = GameConfig.GC_Enemy_Bug_Damage_Quality; break;
            case EnumEnemyType.Sun: damage = GameConfig.GC_Enemy_Sun_Damage_Quality; break;
            case EnumEnemyType.Sport: damage = GameConfig.GC_Enemy_Sport_Damage_Quality; break;
        }

        return damage;
    }

    public void SetSlow()
    {
        SlowUntil = Time.time + GameConfig.GC_Ammo_SlowFor;
    }

    public bool IsSlowed()
    {
        return Time.time < SlowUntil;
    }

}
