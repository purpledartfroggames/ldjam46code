﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] GameObject EnemyGroup;
    [SerializeField] GameObject[] PrefabEnemy;
    [SerializeField] float[] SpawnTimeSinceLast;

    int index = 0;

    // Start is called before the first frame update
    void Start()
    {
        //UGLY!!!!
        Invoke("WaveIncomingNotify4", 0f);
        Invoke("WaveIncomingNotify3", 1f);
        Invoke("WaveIncomingNotify2", 2f);
        Invoke("WaveIncomingNotify1", 3f);
        Invoke("WaveIncomingNotify0", 4f);

        if (PrefabEnemy.Length > 0 && SpawnTimeSinceLast.Length > 0 && PrefabEnemy.Length == SpawnTimeSinceLast.Length)
        {
            this.TriggerSpawnNextEnemy();
        }
        else
        {
            Debug.LogError("Error in enemy spawner setup.");
        }
    }

    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.C))
        {
            this.ClearEnemyGroup();
        }
    }

    private void TriggerSpawnNextEnemy()
    {
        if (index >= PrefabEnemy.Length)
        {
            LevelManager.Instance.SetAllSpawnsed(true);
            return;
        }

        Invoke("SpawnEnemy", SpawnTimeSinceLast[index]);
    }


    private void SpawnEnemy()
    {
        GameObject SpawnedEnemy;

        SpawnedEnemy = Instantiate(PrefabEnemy[index], EnemyGroup.transform);
        SpawnedEnemy.transform.position = this.transform.position;

        AudioManager.Instance.PlaySoundInteraction("EnemySpawn");

        index++;

        this.TriggerSpawnNextEnemy();
    }

    void ClearEnemyGroup()
    {
        foreach (Transform curEnemy in EnemyGroup.transform)
        {
            Destroy(curEnemy.gameObject);
        }
    }

    #region TheUglySection
    private void WaveIncomingNotify4() //UGLY
    {
        PlayerFeedbackManager.Instance.SetFeedbackText("Wave in .. ", 1, false);
    }
    private void WaveIncomingNotify3() //UGLY
    {
        PlayerFeedbackManager.Instance.SetFeedbackText("Wave in .. 3", 2, true);
    }
    private void WaveIncomingNotify2() //UGLY
    {
        PlayerFeedbackManager.Instance.SetFeedbackText("Wave in .. 2", 2, true);
    }
    private void WaveIncomingNotify1() //UGLY
    {
        PlayerFeedbackManager.Instance.SetFeedbackText("Wave in .. 1", 3, true);
    }
    private void WaveIncomingNotify0() //UGLY
    { 
        PlayerFeedbackManager.Instance.SetFeedbackText("Wave in .. 0", 3, true);
    }
    #endregion

    public EnumEnemyType GetLineX(int _number)
    {
        EnumEnemyType CurEnemyType = EnumEnemyType.Unspecified;

        if (PrefabEnemy.Length > index + _number)
        {
            CurEnemyType = PrefabEnemy[index + _number].GetComponent<EnemyController>().GetEnemyType();
        }

        return CurEnemyType;
    }

}
