﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class EndgamePanelController : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI HighestRankCount;
    [SerializeField] TextMeshProUGUI LDRankText;

    void Update()
    {
        this.SetTextValues();
    }

    public void SetTextValues()
    {
        int highestRank = 0;

        if (PlayerPrefs.GetInt("HighScore") != 0)
            highestRank = PlayerPrefs.GetInt("HighScore");

        HighestRankCount.text = ScoreManager.Instance.AddOrdinal(highestRank);
        LDRankText.text = ScoreManager.Instance.GetLDRank() + " of " + GameConfig.GC_LDRank_Participants;

        if (highestRank == ScoreManager.Instance.GetLDRank())
            HighestRankCount.text = HighestRankCount.text + ".. NEW!!";
    }
}
