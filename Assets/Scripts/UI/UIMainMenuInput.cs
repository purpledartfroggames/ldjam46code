﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIMainMenuInput : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        if ((Input.GetKeyUp(KeyCode.Return) || Input.GetKeyUp(KeyCode.KeypadEnter) || Input.GetKeyUp(KeyCode.Alpha2) || Input.GetKeyUp(KeyCode.Keypad2)) && TimeManager.Instance.IsPaused())
        {
            GameManager.Instance.CurCanvasUpdater().StartGame();
        }
    }
}
