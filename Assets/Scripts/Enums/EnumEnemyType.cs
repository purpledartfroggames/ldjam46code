﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EnumEnemyType
{
    Bug,
    Sun,
    Sport,
    Unspecified
}
