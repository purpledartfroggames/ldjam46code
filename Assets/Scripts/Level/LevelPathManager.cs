﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelPathManager : MonoBehaviour
{
    [SerializeField] Transform[] PathPoint;

    public Transform GetPathPoint(int _curIndex)
    {
        if (_curIndex >= PathPoint.Length)
            return null;

        return PathPoint[_curIndex];
    }
}
