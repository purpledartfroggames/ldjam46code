﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    [SerializeField] int MaxHealth;
    [SerializeField] GameObject HealthBar;
    [SerializeField] SpriteRenderer HealthBarRenderer;

    [SerializeField] Color OrigColor;
    [SerializeField] Color SlowedColor;

    [SerializeField] EnemyController MyEnemyController;

    int CurHealth;

    // Start is called before the first frame update
    void Start()
    {
        CurHealth = MaxHealth;
    }

    private void Update()
    {
        if (CurHealth <= 0)
        {
            ScoreManager.Instance.AdjustProjectFocus(1, false, true, this.transform.position.x, this.transform.position.y + 2f, this.transform.position.z);
            ScoreManager.Instance.AdjustProjectQuality(2, false, true, this.transform.position.x, this.transform.position.y + 1f, this.transform.position.z);
            Destroy(this.gameObject);
        }

        if (MyEnemyController.IsSlowed())
            HealthBarRenderer.color = SlowedColor;
        else
            HealthBarRenderer.color = OrigColor;
    }

    public void AdjustHealth(int _adjusment)
    {
        float scale = 1f;

        scale = (float)CurHealth / (float)MaxHealth;

        CurHealth += _adjusment;

        HealthBar.transform.localScale = new Vector3(scale, scale, 0f);
    }

}
