﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CritterLine : MonoBehaviour
{
    [SerializeField] Image line1st;
    [SerializeField] Image line2nd;
    [SerializeField] Image line3rd;
    [SerializeField] Image line4th;
    [SerializeField] Image line5th;
    [SerializeField] Image line6th;

    [SerializeField] Sprite bugSprite;
    [SerializeField] Sprite sunSprite;
    [SerializeField] Sprite sportSprite;
    [SerializeField] Sprite unspecificSprite;

    // Update is called once per frame
    void Update()
    {
        EnemySpawner CurEnemySpawner = GameObject.FindObjectOfType<EnemySpawner>();

        if (CurEnemySpawner != null)
        {
            line1st.sprite = this.SpriteFromType(CurEnemySpawner.GetLineX(0));
            line2nd.sprite = this.SpriteFromType(CurEnemySpawner.GetLineX(1));
            line3rd.sprite = this.SpriteFromType(CurEnemySpawner.GetLineX(2));
            line4th.sprite = this.SpriteFromType(CurEnemySpawner.GetLineX(3));
            line5th.sprite = this.SpriteFromType(CurEnemySpawner.GetLineX(4));
            line6th.sprite = this.SpriteFromType(CurEnemySpawner.GetLineX(5));
        }
    }

    private Sprite SpriteFromType(EnumEnemyType _enemyType)
    {
        Sprite curSprite = unspecificSprite;

        switch (_enemyType)
        {
            case EnumEnemyType.Bug: curSprite = bugSprite; Debug.Log("Bug"); break;
            case EnumEnemyType.Sun: curSprite = sunSprite; Debug.Log("Sun");  break;
            case EnumEnemyType.Sport: curSprite = sportSprite; Debug.Log("Sport"); break;
        }

        return curSprite;
    }
}
