﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateTowardsVelocity : MonoBehaviour
{
    EnemyController myEnemyController;

    //values that will be set in the Inspector
    public Vector2 TargetPosition;
    public float RotationSpeed;

    bool TargetSet;

    //values for internal use
    private Quaternion _lookRotation;
    private Vector2 _direction;

    private void Start()
    {
        myEnemyController = this.GetComponentInParent<EnemyController>();
    }

    private void FixedUpdate()
    {
        this.ApplyPositionAndRotation();
    }


    void ApplyPositionAndRotation()
    {
        TargetPosition = this.transform.position + myEnemyController.GetPathWay();

        Vector2 objectPos = transform.position;
        TargetPosition.x = TargetPosition.x - objectPos.x;
        TargetPosition.y = TargetPosition.y - objectPos.y;

        float angle = Mathf.Atan2(TargetPosition.y, TargetPosition.x) * Mathf.Rad2Deg;
        angle = angle - 90 + Random.Range(0, GameConfig.GC_EnemyMove_Static);
        //transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle));
        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(new Vector3(0, 0, angle)), RotationSpeed);
    }

    // Update is called once per frame
    /*
    void Update()
    {
        if (TargetSet == true)
        {
            //find the vector pointing from our position to the target
            _direction = (TargetPosition - (Vector2) transform.position).normalized;

            //create the rotation we need to be in to look at the target
            _lookRotation = Quaternion.LookRotation(_direction);

            //rotate us over time according to speed until we are in the required rotation
            transform.rotation = Quaternion.Slerp(transform.rotation, _lookRotation, Time.deltaTime * RotationSpeed);
        }
        else
        {
            if (myEnemyController != null)
            {
                TargetPosition = this.transform.position + myEnemyController.GetPathWay();
                TargetSet = true;
            }
            else
            {
                TargetSet = false;
            }
        }
    }
    */
}
