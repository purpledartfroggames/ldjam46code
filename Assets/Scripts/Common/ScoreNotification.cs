﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ScoreNotification : MonoBehaviour
{
    [SerializeField] Color ColorUp;
    [SerializeField] Color ColorDown;
    [SerializeField] TextMeshPro Text;

    // Start is called before the first frame update
    void Start()
    {
        Debug.LogWarning("Notification!");
        Destroy(this.gameObject, GameConfig.GC_System_Notification_Life);        
    }

    private void Update()
    {
        Vector3 moveVector = Vector2.up;
        Vector3 newPos = this.transform.position + (moveVector * GameConfig.GC_System_Notification_Speed);

        this.transform.position = newPos;
    }

    public void UpdateText(string _notificationText, bool _positive)
    {
        Text.color = _positive == true ? ColorUp : ColorDown;
        Text.text = _notificationText;
    }
}
