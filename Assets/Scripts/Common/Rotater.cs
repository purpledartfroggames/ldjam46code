﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotater : MonoBehaviour
{
    [SerializeField] float Angle;

    // Update is called once per frame
    void FixedUpdate()
    {
        this.transform.Rotate(new Vector3(0f, 0f, 1f), Angle, Space.Self);
    }

    public void SetRotation(float _rotation)
    {
        Angle = _rotation;
    }
}
