﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NotificationManager : MonoBehaviour
{
    public static NotificationManager Instance;

    [SerializeField] GameObject PrefabNotification;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
            return;
        }

        //DontDestroyOnLoad(this.gameObject);
    }

    public void Notify(string _notificationText, bool _positive, bool _override, Vector3 _overridePos, int _value = 0)
    {
        GameObject Notification = Instantiate(PrefabNotification);

        if (Notification != null)
        {
            ScoreNotification MyScoreNotification = Notification.GetComponent<ScoreNotification>();

            Notification.transform.position = new Vector3(9f, 0f, 0f);

            if (_override == true)
                Notification.transform.position = _overridePos;

            if (MyScoreNotification != null)
                MyScoreNotification.UpdateText(_value.ToString() + " " + _notificationText, _positive);
        }
    }
}
