﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeManager : MonoBehaviour
{
    public static TimeManager Instance;

    [SerializeField] bool GamePaused = true;

    public void ResetManager()
    {
    }

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }

        //DontDestroyOnLoad(Instance);
    }

    public void SetPaused(bool _mustPause)
    {
        if (_mustPause == true)
        {
            GamePaused = true;
            Time.timeScale = 0f;
        }
        else
        {
            GamePaused = false;
            Time.timeScale = 1f;
        }
    }

    public void AdjustTime(float _change)
    {
        string uglyGameSpeedIndicator = "";

        if (Time.timeScale <= 1f && _change < 0)
            _change = _change / 4f;
        else if (Time.timeScale < 1f && _change > 0)
            _change = _change / 4f;

        Time.timeScale = Mathf.Clamp(Time.timeScale + _change ,0, 10);

        uglyGameSpeedIndicator = "Unknown speed";

        if (Time.timeScale == 0f)
            uglyGameSpeedIndicator = "Pause";
        else if (Time.timeScale == .25f)
            uglyGameSpeedIndicator = "Ultra slow";
        else if (Time.timeScale == .5f)
            uglyGameSpeedIndicator = "Very slow";
        else if (Time.timeScale == .75f)
            uglyGameSpeedIndicator = "Little slow";
        else if (Time.timeScale == 1f)
            uglyGameSpeedIndicator = "Normal";
        else if (Time.timeScale == 2f)
            uglyGameSpeedIndicator = "Quick";
        else if (Time.timeScale == 3f)
            uglyGameSpeedIndicator = "Faster";
        else if (Time.timeScale == 4f)
            uglyGameSpeedIndicator = "Very fast";
        else if (Time.timeScale == 5f)
            uglyGameSpeedIndicator = "Even faster";
        else if (Time.timeScale == 6f)
            uglyGameSpeedIndicator = "Damn...";
        else if (Time.timeScale == 7f)
            uglyGameSpeedIndicator = "Take it easy";
        else if (Time.timeScale == 8f)
            uglyGameSpeedIndicator = "Forget it ... meep meep";
        else if (Time.timeScale == 9f)
            uglyGameSpeedIndicator = "Blistering speed";
        else if (Time.timeScale == 10f)
            uglyGameSpeedIndicator = "Lightspeed";

        PlayerFeedbackManager.Instance.SetFeedbackText(uglyGameSpeedIndicator, 1, true);
    }

    public bool IsPaused()
    {
        return GamePaused;
    }
}
