﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    static public LevelManager Instance;

    [SerializeField] GameObject[] Levels;
    [SerializeField] GameObject EnemyGroup;
    [SerializeField] GameObject TowerGroup;
    [SerializeField] GameObject AmmoGroup;

    float TimeInSession = 0f;

    int curLevel = 0;
    int nextLevel = 0;

    bool LevelAllSpawned = false;
    bool GameOver = false;

    public void ResetManager()
    {
        GameOver = false;
        
    }

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
            return;
        }

        //DontDestroyOnLoad(this.gameObject);
    }

    private void Update()
    {
        TimeInSession += Time.deltaTime;

        if (GameOver == false
        &&  EnemyGroup != null
        && LevelAllSpawned == true
        &&  EnemyGroup.transform.childCount == 0)
        {
            LevelManager.Instance.LaunchNextLevel();
        }
    }

    public void LaunchNextLevel()
    {
        TutorialManager.Instance.SetTutorialActive(nextLevel == 0);

        if (nextLevel == 0)
        {
            TimeInSession = 0f;
        }

        if (nextLevel < Levels.Length)
        {
            ScoreManager.Instance.AdjustProjectFocus(GameConfig.GC_Level_Completion_Bonus, false);

            curLevel++;
            this.ClearAmmoGroup();
            this.ClearTowerGroup();
            this.SetAllSpawnsed(false);
            Levels[Mathf.Clamp(nextLevel - 1, 0, 10)].active = false;
            Levels[nextLevel].active = true;
            nextLevel++;
            GameObject.FindObjectOfType<UIMenuController>().SetLevelText();
        }
        else
        {
            GameObject.FindObjectOfType<UIMenuController>().EndGame(true, "You cleared all levels!");
            GameObject.FindObjectOfType<EndgamePanelController>().SetTextValues();
        }
    }

    public void SetAllSpawnsed(bool _firstSpawns)
    {
        LevelAllSpawned = _firstSpawns;
    }

    void ClearTowerGroup()
    {
        foreach (Transform curTower in TowerGroup.transform)
        {
            Destroy(curTower.gameObject);
        }
    }

    void ClearAmmoGroup()
    {
        foreach (Transform curAmmo in AmmoGroup.transform)
        {
            Destroy(curAmmo.gameObject);
        }
    }

    void ClearEnemyGroup()
    {
        foreach (Transform curEnemy in EnemyGroup.transform)
        {
            Destroy(curEnemy.gameObject);
        }
    }

    void HideAllLevels()
    {
        foreach (GameObject curLevel in Levels)
        {
            curLevel.active = false;
        }
    }

    public void ClearAllGroupsAndHideInfo()
    {
        this.ClearAmmoGroup();
        this.ClearEnemyGroup();
        this.ClearTowerGroup();

        this.HideAllLevels();

        PlayerFeedbackManager.Instance.SetFeedbackText("", 1, true);
    }

    public void SetGameOver(bool _gameOver)
    {
        GameOver = _gameOver;
    }

    public bool GetGameGameOver()
    {
        return GameOver;
    }


    public int GetCurLevel()
    {
        return curLevel;
    }

    public float GetTimeInSession()
    {
        return TimeInSession;
    }

}
