﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    static public ScoreManager Instance;

    int ProjectFocus;
    int ProjectQuality;

    public void ResetManager()
    {
        this.AdjustProjectFocus(GameConfig.GC_Game_ProjectFocus, true);
        this.AdjustProjectQuality(GameConfig.GC_Game_ProjectQuality, true);
    }

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;

            this.ResetManager();
        }
        else
        {
            Destroy(this);
            return;
        }

        //DontDestroyOnLoad(this.gameObject);
    }

    private void Update()
    {
    }


    public int GetProjectFocus()
    {
        return ProjectFocus;
    }

    public int GetProjectQuality()
    {
        return ProjectQuality;
    }

    public void AdjustProjectFocus(int _adjustment, bool _hideNotify, bool _override = false, float _x = 0f, float _y = 0f, float _z = 0f)
    {
        ProjectFocus = Mathf.Clamp(ProjectFocus + _adjustment, -1, GameConfig.GC_Game_ProjectFocus);
        GameObject.FindObjectOfType<UIMenuController>().AdjustSliders();

        if (ProjectFocus < 0)
        {
            GameObject.FindObjectOfType<UIMenuController>().EndGame(false, "You lost too much project focus");
        }

        if (_hideNotify == false)
            NotificationManager.Instance.Notify("Focus", _adjustment > 0, _override, new Vector3(_x, _y, _z), _adjustment);
    }

    public void AdjustProjectQuality(int _adjustment, bool _hideNotify, bool _override = false, float _x = 0f, float _y = 0f, float _z = 0f)
    {
        ProjectQuality = Mathf.Clamp(ProjectQuality + _adjustment, -1, GameConfig.GC_Game_ProjectQuality);
        GameObject.FindObjectOfType<UIMenuController>().AdjustSliders();

        if (ProjectQuality < 0)
        {
            GameObject.FindObjectOfType<UIMenuController>().EndGame(false, "You lost too much project quality");
        }

        if (_hideNotify == false)
            NotificationManager.Instance.Notify("Quality", _adjustment > 0, _override, new Vector3(_x, _y, _z), _adjustment);
    }

    public int GetLDRank()
    {
        float MaxQualityMod = GameConfig.GC_LDRank_MaxQuality;
        float MaxFocus = GameConfig.GC_LDRank_MaxFocus;
        float MaxScore = (MaxQualityMod + MaxFocus) * 100; //"Multiply" with nummerical percentage from time
        
        float AchievedScore = (ScoreManager.Instance.GetProjectFocus() + (ScoreManager.Instance.GetProjectQuality() * GameConfig.GC_LDRank_MaxQualityModifier)) * 100;
        
        float TimeOverspend = Mathf.Clamp(Time.time - GameConfig.GC_LDRank_ExpectedTime, 0, GameConfig.GC_LDRank_ExpectedTimeMaxPenalty);
        float TimeMaxPenalty = GameConfig.GC_LDRank_ExpectedTimeMaxPenalty;
        float CalculateTimeModifier = GameConfig.GC_LDRank_ExpectedTimeMaxPenalty / (GameConfig.GC_LDRank_ExpectedTimeMaxPenalty - TimeOverspend) * 100;

        float AchievedScoreMod = AchievedScore * CalculateTimeModifier;

        float CalculatedPercent = (MaxScore - AchievedScore) / MaxScore * 100;
        int AchievedRank = (int) Mathf.Clamp((GameConfig.GC_LDRank_Participants * CalculatedPercent) / 100, 1, GameConfig.GC_LDRank_Participants);

        //float TimeFactor = Mathf.Clamp(Time.time - GameConfig.GC_LDRank_ExpectedTime, 0, GameConfig.GC_LDRank_ExpectedTimeMaxPenalty) / 10 * GameConfig.GC_LDRank_PenaltyPerExtra10Sec;

        this.AddOrdinal(AchievedRank);

        return AchievedRank;
    }

    public string GetLDRankString()
    {
        return this.AddOrdinal(this.GetLDRank());
    }

    public string AddOrdinal(int num)
    {
        if (num <= 0) return num.ToString();

        switch (num % 100)
        {
            case 11:
            case 12:
            case 13:
                return num + "th";
        }

        switch (num % 10)
        {
            case 1:
                return num + "st";
            case 2:
                return num + "nd";
            case 3:
                return num + "rd";
            default:
                return num + "th";
        }
    }
}
