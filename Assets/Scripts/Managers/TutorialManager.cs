﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TutorialManager : MonoBehaviour
{
    static public TutorialManager Instance;

    bool TutorialActive;

    bool TutorialPart01Teached;
    bool TutorialPart02Teached;

    [Header("Panel")]
    [SerializeField] GameObject TutorialPanel;

    [Header("Colors")]
    [SerializeField] Color colorDone;
    [SerializeField] Color colorRemain;

    [Header("Tutorials")]
    [SerializeField] TextMeshProUGUI TutorialPart01;
    [SerializeField] TextMeshProUGUI TutorialPart02;

    public void ResetManager()
    {
        TutorialPart01Teached = false;
        TutorialPart02Teached = false;
    }

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
            return;
        }

        //DontDestroyOnLoad(this.gameObject);
    }

    #region Status
    public bool TutorialFinished()
    {
        bool IsTutorialFinished;

        IsTutorialFinished = TutorialPart01Teached && TutorialPart02Teached; //TODO: And more?

        return IsTutorialFinished;
    }
    #endregion

    public bool GetTutorialPart01Teached() { return TutorialPart01Teached;  }
    public bool GetTutorialPart02Teached() { return TutorialPart02Teached; }

    public void SetTutorialPart01Teached(bool _tutorialTeached)
    {
        TutorialPart01Teached = _tutorialTeached;
        AudioManager.Instance.PlaySoundInteraction("Tut01Teached");
        TutorialPart01.color = colorDone;
        PlayerFeedbackManager.Instance.SetFeedbackText("Tutorial 01 done", 1, false);
    }

    public void SetTutorialPart02Teached(bool _tutorialTeached)
    {
        TutorialPart02Teached = _tutorialTeached;
        AudioManager.Instance.PlaySoundInteraction("Tut02Teached");
        TutorialPart02.color = colorDone;
        PlayerFeedbackManager.Instance.SetFeedbackText("Tutorial 02 done", 1, false);
    }

    public void SetTutorialPanelActive(bool _active)
    {
       TutorialPanel.SetActive(_active && TutorialActive);
    }

    public void SetTutorialActive(bool _active)
    {
        if (_active == true)
        {
            TimeManager.Instance.SetPaused(true);   
            PlayerFeedbackManager.Instance.SetFeedbackText("Build First tower.", 2, false, 30f);
            PlayerFeedbackManager.Instance.SetFeedbackText("ALL Towers free in tutorial", 2, false, 30f);
            PlayerFeedbackManager.Instance.SetFeedbackText("P unpauses", 1, false, 30f);
        }

        TutorialActive = _active;

        if (TutorialActive == true && _active == false)
        {
            ScoreManager.Instance.ResetManager();
        }
    }

    public bool GetTutorialActive()
    {
        return TutorialActive;
    }

}
