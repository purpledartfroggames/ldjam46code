﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    static public GameManager Instance;

    [Header("Misc")]
    [SerializeField] UIMenuController MenuUpdater;

    [Header("Ingame objects")]
    [SerializeField] Camera GameCamera;

    GameObject PlayerGO;

    [SerializeField] GameObject TitleTextGO;
    [SerializeField] GameObject UserInterface;
    [SerializeField] GameObject TileMaps;

    int PlayerPoints;
    int PointsAchieved;

    public void ResetManager()
    {
        PlayerPoints = 0;
        PointsAchieved = 0;
    }

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
            return;
        }

        //DontDestroyOnLoad(this.gameObject);
    }

    private void Update()
    {
    }

    public GameObject CurPlayerGO()
    {
        return PlayerGO;
    }

    public UIMenuController CurCanvasUpdater()
    {
        return MenuUpdater;
    }

    public void ResetLevel()
    {
    }

    public float DistanceToPlayerVolume(Vector2 _position)
    {
        float MaxDistance = 20f;
        float distance;

        if (PlayerGO != null)
            distance = Mathf.Clamp(Vector2.Distance(_position, PlayerGO.transform.position), 0f, MaxDistance);
        else
            distance = 5f;

        float volume;

        volume = Mathf.Clamp(((MaxDistance - distance) / MaxDistance), 0f, 1f);

        return volume;
    }

    public void ShowGameObjects(bool _doShow)
    {
    }

    public void AdjustPoints(int _adjustment)
    {
        PlayerPoints += _adjustment;
        PointsAchieved = PlayerPoints;

    }

    public int CurPoints()
    {
        return PlayerPoints;
    }

    public void ResetManagers()
    {
        GameManager.Instance.ResetManager();
        LevelManager.Instance.ResetManager();
        TimeManager.Instance.ResetManager();
        TutorialManager.Instance.ResetManager();
        ScoreManager.Instance.ResetManager();
    }

    public void ClearPointsAchieved()
    {
        PointsAchieved = 0;
    }
}
