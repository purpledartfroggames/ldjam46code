﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoController : MonoBehaviour
{
    [SerializeField] float AmmoSpeed;
    GameObject Target;
    Vector3 TargetPos;
    Vector3 pathWay;

    [SerializeField] int DamageVsBugs;
    [SerializeField] int DamageVsSun;
    [SerializeField] int DamageVsSports;

    [SerializeField] EnumAmmoType MyAmmoType;

    // Update is called once per frame
    void Update()
    {
        this.MoveOnPath();
        Destroy(this.gameObject, GameConfig.GC_Ammo_AliveFor);
    }

    void MoveOnPath()
    {
        if (AmmoSpeed == 0f)
        {
            Debug.LogWarning("Ammo with no speed");
            return;
        }

        if (TargetPos != Vector3.zero)
        {
            pathWay = TargetPos - this.transform.position;
            pathWay = pathWay.normalized;
            this.transform.Translate(pathWay * AmmoSpeed * Time.deltaTime);
        }
    }

    public void SetTarget(GameObject _target)
    {
        Target = _target;
        TargetPos = _target.transform.position;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            Health EnemyHealth = collision.gameObject.GetComponent<Health>();
            EnemyController CurEnemyController = collision.gameObject.GetComponent<EnemyController>();

            if (EnemyHealth != null && CurEnemyController != null) 
            {
                int damage = 0;

                switch (CurEnemyController.GetEnemyType())
                {
                    case EnumEnemyType.Bug: damage = DamageVsBugs; break;
                    case EnumEnemyType.Sun: damage = DamageVsSun; break;
                    case EnumEnemyType.Sport: damage = DamageVsSports; break;
                }
                
                EnemyHealth.AdjustHealth(-damage);

                if (MyAmmoType == EnumAmmoType.Slow)
                {
                    CurEnemyController.SetSlow();
                }
            }

            Destroy(this.gameObject);
        }
    }

}
