﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerController : MonoBehaviour
{
    [SerializeField] GameObject EnemyGroup;
    [SerializeField] GameObject AmmoGroup;
    [SerializeField] float Range;
    [SerializeField] GameObject PrefabAmmo;
    [SerializeField] float FireRate;
    float NextFire;
    [SerializeField] EnumTowerType TowerType;

    GameObject NearestEnemy;
    float NearestRange;

    // Start is called before the first frame update
    void Start()
    {
        EnemyGroup = GameObject.Find("EnemyGroup");
        AmmoGroup = GameObject.Find("AmmoGroup");

        this.InitRangeFromType();

        InvokeRepeating("TrackNearestEnemy", 1f, 0.1f);
    }

    // Update is called once per frame
    void Update()
    {
        this.FireTurret();
    }

    private void FireTurret()
    {
        if (NearestEnemy != null)
        {
            if (Time.time > NextFire)
            {
                GameObject AmmoGO = Instantiate(PrefabAmmo, AmmoGroup.transform);
                AmmoController AmmoGOAmmoController = AmmoGO.GetComponent<AmmoController>();
                AmmoGO.transform.position = this.transform.position;
                AudioManager.Instance.PlaySoundInteractionVolumeRandomPitch("TowerFire", 1f);

                if (AmmoGOAmmoController != null)
                {
                    AmmoGOAmmoController.SetTarget(NearestEnemy);
                }

                NextFire = Time.time + FireRate;
            }
        }
    }

    void TrackNearestEnemy()
    {
        NearestRange = 0f;
            
        foreach (Transform curEnemy in EnemyGroup.transform)
        {
            if (NearestEnemy == null)
            {
                this.Target(curEnemy, Vector2.Distance(curEnemy.transform.position, this.transform.position));
            }

            if (Vector2.Distance(curEnemy.transform.position, this.transform.position) < NearestRange)
            {
                this.Target(curEnemy, Vector2.Distance(curEnemy.transform.position, this.transform.position));
            }
        }
    }

    private void Target(Transform curEnemy, float CurRange)
    {
        if (CurRange <= Range)
        {
            NearestRange = CurRange;
            NearestEnemy = curEnemy.gameObject;
        }
    }

    public int GetCost()
    {
        int cost = 0;

        switch (TowerType)
        {
            case EnumTowerType.Fixer: cost = GameConfig.GC_Tower_Cost_Fixer; break;
            case EnumTowerType.Zone: cost = GameConfig.GC_Tower_Cost_Zone; break;
            case EnumTowerType.Coffenator: cost = GameConfig.GC_Tower_Cost_Coffenator; break;
        }

        return cost;
    }

    public void InitRangeFromType()
    {
        switch (TowerType)
        {
            case EnumTowerType.Fixer: Range = GameConfig.GC_Tower_Range_Fixer; break;
            case EnumTowerType.Zone: Range = GameConfig.GC_Tower_Range_Zone; break;
            case EnumTowerType.Coffenator: Range = GameConfig.GC_Tower_Range_Coffenator; break;
        }
    }
}
