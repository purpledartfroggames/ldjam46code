﻿    using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.KeypadPlus))
        {
            TimeManager.Instance.AdjustTime(1);
        }
        if (Input.GetKeyUp(KeyCode.KeypadMinus))
        {
            TimeManager.Instance.AdjustTime(-1);
        }
        if (Input.GetKeyUp(KeyCode.D))
        {
            PlayerPrefs.DeleteAll();
        }
        if (Input.GetKeyUp(KeyCode.L))
        {
            GameObject.FindObjectOfType<UIMenuController>().EndGame(false, "You forfeited!", true);
        }
        if (Input.GetKeyUp(KeyCode.P))
        {
            TimeManager.Instance.SetPaused(!TimeManager.Instance.IsPaused());
        }
        if (Input.GetKeyUp(KeyCode.M))
        {
            AudioManager.Instance.ToggleMusic();
        }
        if (Input.GetKeyUp(KeyCode.R))
        {
            GameManager.Instance.CurCanvasUpdater().RestartGame();
        }

        if (Input.GetKeyUp(KeyCode.KeypadEnter) || Input.GetKeyUp(KeyCode.Return))
        {
            GameObject.FindObjectOfType<UIMenuController>().ResumeGame();
        }
    }
}
