﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerBuildController : MonoBehaviour
{
    [SerializeField] GameObject TowerGroup;
    [SerializeField] GameObject[] PrefabTowers;

    [SerializeField] LayerMask LayerMaskGrid;
    [SerializeField] LayerMask LayerMaskGridTowers;

    [SerializeField] GameObject IndicatorClear;
    [SerializeField] GameObject IndicatorSell;
    [SerializeField] GameObject IndicatorFixer;
    [SerializeField] GameObject IndicatorZone;
    [SerializeField] GameObject IndicatorCoffenator;

    [SerializeField] GameObject IndicatorLevel0;
    [SerializeField] GameObject IndicatorLevel1;
    [SerializeField] GameObject IndicatorLevel2;
    [SerializeField] GameObject IndicatorLevel3;
    [SerializeField] GameObject IndicatorLevel4;

    GameObject PrefabTowerActive;

    bool SellMode = false;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Alpha0) || Input.GetKeyUp(KeyCode.Keypad0)
        || Input.GetKeyUp(KeyCode.Tilde) || Input.GetMouseButtonUp(1))                                             //DESELECT
            this.DeActivatePrefabAndClear();
        if (PrefabTowers.Length >= 1 && (Input.GetKeyUp(KeyCode.Alpha1) || Input.GetKeyUp(KeyCode.Keypad1)))        //FIXER
            this.ActivatePrefab(0);
        else if (PrefabTowers.Length >= 2 && (Input.GetKeyUp(KeyCode.Alpha2) || Input.GetKeyUp(KeyCode.Keypad2)))   //ZONE
            this.ActivatePrefab(1);
        else if (PrefabTowers.Length >= 3 && (Input.GetKeyUp(KeyCode.Alpha3) || Input.GetKeyUp(KeyCode.Keypad3)))   //COFFEE
            this.ActivatePrefab(2);
        else if (Input.GetKeyUp(KeyCode.Q))                                                                         //SELL
            this.DeActivatePrefabAndSell();
    }

    private void ShowBuildable(bool _show)
    {
        IndicatorLevel0.active = _show && LevelManager.Instance.GetCurLevel() == 1;
        IndicatorLevel1.active = _show && LevelManager.Instance.GetCurLevel() == 2;
        IndicatorLevel2.active = _show && LevelManager.Instance.GetCurLevel() == 3;
        IndicatorLevel3.active = _show && LevelManager.Instance.GetCurLevel() == 4;
        IndicatorLevel4.active = _show && LevelManager.Instance.GetCurLevel() == 5;
    }

    public void ActivatePrefab(int _prefabIdx)
    {
        SellMode = false;

        IndicatorClear.SetActive(false);
        IndicatorSell.SetActive(false);
        IndicatorFixer.SetActive(_prefabIdx == 0);
        IndicatorZone.SetActive(_prefabIdx == 1);
        IndicatorCoffenator.SetActive(_prefabIdx == 2);

        PrefabTowerActive = PrefabTowers[_prefabIdx];

        this.ShowBuildable(true);
    }

    public void DeActivatePrefabAndSell()
    {
        SellMode = true;

        IndicatorClear.SetActive(false);
        IndicatorSell.SetActive(true);
        IndicatorFixer.SetActive(false);
        IndicatorZone.SetActive(false);
        IndicatorCoffenator.SetActive(false);

        PrefabTowerActive = null;

        this.ShowBuildable(false);
    }

    public void DeActivatePrefabAndClear()
    {
        SellMode = false;

        IndicatorClear.SetActive(true);
        IndicatorSell.SetActive(false);
        IndicatorFixer.SetActive(false);
        IndicatorZone.SetActive(false);
        IndicatorCoffenator.SetActive(false);

        PrefabTowerActive = null;

        this.ShowBuildable(false);
    }


    public void BuildActiveTowerPrefab(Vector3 _mousePos)
    {
        if (PrefabTowerActive != null)
        {
            Vector3 clickPoint = Camera.main.ScreenToWorldPoint(_mousePos);
          
            Collider2D[] colliders = Physics2D.OverlapCircleAll(clickPoint, GameConfig.GC_Build_Tolerance, LayerMaskGrid);
            Debug.DrawLine(clickPoint, Vector2.down, Color.green);

            foreach (Collider2D curCollider in colliders)
            {
                int cost = this.GetTowerCost();

                if (ScoreManager.Instance.GetProjectFocus() >= cost || TutorialManager.Instance.GetTutorialActive() == true)
                {
                    AudioManager.Instance.PlaySoundInteractionVolumeRandomPitch("TowerBuild", 1f);

                    this.BuildTower(curCollider.transform.position);
                    ScoreManager.Instance.AdjustProjectFocus(-cost, false);
                }
            }
        }
        else if (SellMode == true)
        {
            Vector3 clickPoint = Camera.main.ScreenToWorldPoint(_mousePos);

            RaycastHit2D[] colliders = Physics2D.RaycastAll(clickPoint, Vector2.down, GameConfig.GC_Build_Tolerance, LayerMaskGridTowers);
            Debug.DrawLine(clickPoint, Vector2.down, Color.red);

            foreach (RaycastHit2D curCollider in colliders)
            {
                if (curCollider.transform.gameObject.tag == "Tower")
                {
                    int refund = this.GetTowerRefund(curCollider.transform.gameObject);

                    AudioManager.Instance.PlaySoundInteractionVolumeRandomPitch("TowerSell", 1f);

                    Destroy(curCollider.transform.gameObject);
                    ScoreManager.Instance.AdjustProjectFocus(refund, false);
                }
            }
        }
    }

    private int GetTowerCost()
    {
        TowerController MyTowerController;
        int cost = 0;

        MyTowerController = PrefabTowerActive.GetComponent<TowerController>();

        if (MyTowerController != null)
        {
            cost = MyTowerController.GetCost();
        }

        return cost;
    }

    private int GetTowerRefund(GameObject _curGO)
    {
        TowerController MyTowerController;
        int cost = 0;

        MyTowerController = _curGO.GetComponent<TowerController>();

        if (MyTowerController != null)
        {
            cost = (int) ((float) MyTowerController.GetCost() / 2f);
        }

        return cost;
    }

    private void BuildTower(Vector3 _construcPos)
    {
        if (TowerGroup != null && this.ValidateNotExistingTower(_construcPos) == false)
        {
            GameObject Tower = Instantiate(PrefabTowerActive, TowerGroup.transform);

            Tower.transform.position = _construcPos;
        }
    }

    private bool ValidateNotExistingTower(Vector3 _requestedPos)
    {
        bool Existing = false;

        foreach (Transform curTower in TowerGroup.transform)
        {
            Existing = (curTower.position == _requestedPos) || Existing;
        }

        return Existing;
    }
}
