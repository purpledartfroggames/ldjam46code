﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseController : MonoBehaviour
{
    TowerBuildController CurTowerBuildController;

    // Start is called before the first frame update
    void Start()
    {
        CurTowerBuildController = GameObject.FindObjectOfType<TowerBuildController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonUp(0))
        {
            this.BuildTower();
        }
    }

    void BuildTower()
    {
        if (CurTowerBuildController != null)
        {
            CurTowerBuildController.BuildActiveTowerPrefab(Input.mousePosition);
        }
    }
}
