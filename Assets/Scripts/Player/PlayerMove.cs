﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    Vector2 moveVector;

    [SerializeField] Rigidbody2D myRigibody;

    [SerializeField] float smoothTime;

    Vector2 MyVelocity = Vector2.zero;

    bool movedLeft, movedRight, movedUp, movedDown;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    private void Update()
    {
        moveVector = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
    }

    private void TutorialExample()
    {
        if (TutorialManager.Instance.GetTutorialActive() == true)
        {
            if (TutorialManager.Instance.GetTutorialPart01Teached() == false)
            {
                TutorialManager.Instance.SetTutorialPart01Teached(true);
            }
        }
    }

    private void FixedUpdate()
    {
        myRigibody.MovePosition(Vector2.SmoothDamp(myRigibody.transform.position, (Vector2)myRigibody.transform.position + moveVector, ref MyVelocity, smoothTime));
    }
}
