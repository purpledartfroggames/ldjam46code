﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

static public class GameConfig
{
    static public int GC_Game_ProjectFocus = 20;
    static public int GC_Game_ProjectFocusMax = 30;
    static public int GC_Game_ProjectQuality = 100;

    static public float GC_Camera_Speed = .05f;
    static public float GC_Camera_LockOuterPosition = 5f;

    static public float GC_Setup_PathDistanceTolerance = .05f;
    static public float GC_Build_Tolerance = 0.1f;


    static public float GC_Ammo_AliveFor = 2f;
    static public float GC_Ammo_SlowFor = 6f;
    static public float GC_Ammo_SlowFactor = .3f;
    static public float GC_EnemyMove_Static = 5f;

    static public int GC_Enemy_Bug_Damage_Focus = 1;
    static public int GC_Enemy_Sun_Damage_Focus = 2;
    static public int GC_Enemy_Sport_Damage_Focus = 3;

    static public int GC_Enemy_Bug_Damage_Quality = 10;
    static public int GC_Enemy_Sun_Damage_Quality = 15;
    static public int GC_Enemy_Sport_Damage_Quality = 20;


    static public int GC_Tower_Cost_Fixer = 2;
    static public int GC_Tower_Cost_Zone = 5;
    static public int GC_Tower_Cost_Coffenator = 6;

    static public float GC_Tower_Range_Fixer = 6f;
    static public float GC_Tower_Range_Zone = 4f;
    static public float GC_Tower_Range_Coffenator = 5f;

    static public int GC_LDRank_Participants = 10000 + Random.Range(-1000, 1000);
    static public int GC_LDRank_MaxQualityModifier = 3;
    static public int GC_LDRank_MaxQuality = GC_Game_ProjectQuality * GC_LDRank_MaxQualityModifier;
    static public int GC_LDRank_MaxFocus = GC_Game_ProjectFocusMax;
    static public int GC_LDRank_ExpectedTime = 420;
    static public int GC_LDRank_ExpectedTimeMaxPenalty = 420; //The mini golf system
    static public int GC_LDRank_PenaltyPerExtra10Sec = 2;

    static public float GC_System_Notification_Life = 1.5f;
    static public float GC_System_Notification_Speed = 0.01f;

    static public int GC_Level_Completion_Bonus = 10;
}
