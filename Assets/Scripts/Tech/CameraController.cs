﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController: MonoBehaviour
{
    private void Update()
    {
        Vector3 moveVector = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), 0f).normalized;
        Vector3 newPos = this.transform.position + (moveVector * GameConfig.GC_Camera_Speed);

        newPos = new Vector3(Mathf.Clamp(newPos.x, -GameConfig.GC_Camera_LockOuterPosition, GameConfig.GC_Camera_LockOuterPosition),
                             Mathf.Clamp(newPos.y, -GameConfig.GC_Camera_LockOuterPosition, GameConfig.GC_Camera_LockOuterPosition),
                             Mathf.Clamp(newPos.z, -GameConfig.GC_Camera_LockOuterPosition, GameConfig.GC_Camera_LockOuterPosition));


        this.transform.position = newPos;
    }
}
